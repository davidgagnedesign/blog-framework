# Blog framework

Blog framework made to explore possibilities with HTML custom elements and PWA

## Client side

The client side is a collection of HTML custom elements, services and helpers that build the different parts of the application.

### Components
The component are made using custom HTML element render in the shadow DOM. Each components consists of a minimum of 3 files.
- index.js (The component javaScript Class instance)
- markup.js (A javascript function which return the HTML string template of the element)
- style.js  (A javascript function which return the STYLE tag string template of the element)

All components must be imported in the `./src/index.js` file to be build via webpack.

All components's helpers or shared functions are in the `./src/core/index.js` file (EX: `InitTemplate()` which create the element).

The components in the pages folder, represent page building components used by the router. The index of the pages folder is the router for the different pages.

#### Components javaScript class template

> The `buildElement()` method is require for the template to render it is use by the `InitTemplate` function in the `./src/components/index.js` file.

```
import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';

class Navigation extends HTMLElement {
    constructor() {
        super();
        InitTemplate(this);
    }

    buildElement() {
        
        let viewData = this.getData();

        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getData() {
        return 'Whatever the require data is';
    }

}

window.customElements.define('blog-nav', Navigation);
```

#### Component Markup.js template
```
export default function (viewData = null) {
    return /* html */`
        <h1 class="main-title">${viewData.title}</h1>
    `;
}
```

#### Component Style.js template
```
export default /* html */`
    <style>
        h1 {
            color: green;
        }
    </style>
`;
```