import { handleFirstLoad } from './core/router';

import './components/title';
import './components/menu';
import './components/links';
import './components/post-card';

import './pages/index';
import './pages/home';
import './pages/about';
import './pages/articles';
import './pages/post';

handleFirstLoad();