export default function (viewData = null) {
    return /* html */`
        <section id="presentation">
            ${viewData.presentation}
        </section>
        <section id="posts">
            ${viewData.lastestPost.map((item, i) => `
                <post-card
                    postid="${item.id}" 
                    title="${item.title}"
                    shortcontent="${item.shortContent}" 
                    postdate="${item.published}">
                </post-card>
            `).join('')}
        </section>
    `;
}