import { InitTemplate } from '../../core/template';
import Posts from '../../../static/data/posts';
import Style from './style';
import Markup from './markup';

class HomePage extends HTMLElement {
    constructor() {
        super();
        InitTemplate(this);
    }
    
    buildElement() {

        let viewData = {
            presentation: this.getPresentationText(),
            lastestPost: this.getLatestPosts()
        }

        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getPresentationText() {
        return 'This blog was developed to explore the capabilities of custom web elements and Progressive web app. It been used as a learning tool and maybe in the future will become a real blog framework that is lightweight and versatile.'
    }

    getLatestPosts(limit = 3) {
        return Posts;
    }
}

window.customElements.define('home-page', HomePage);