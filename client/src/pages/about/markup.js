export default function (viewData = null) {
    return /* html */`
        <section>${viewData.text}</section>
    `;
}