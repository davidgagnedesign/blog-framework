import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';

class AboutPage extends HTMLElement {
    constructor() {
        super();
        InitTemplate(this);
    }

    buildElement() {
        
        let viewData = {
            text: this.getText()
        }

        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getText() {
        return 'Text for the about page';
    }

}

window.customElements.define('about-page', AboutPage);