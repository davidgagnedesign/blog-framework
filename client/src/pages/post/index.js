import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';
import post from '../../../static/data/posts';

class PostPage extends HTMLElement {
    constructor() {
        super();
        this.isInit = false;
    }

    static get observedAttributes() {
        return ['post-id'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'post-id') this.postId = newValue;
        !this.isInit ? this.initElement() : this.shadowRoot.innerHTML = this.buildElement();
    }

    initElement() {
        InitTemplate(this);
        this.isInit = true;
    }

    buildElement() {
        
        let viewData = this.getText();
        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getText() {
        return post.find((element) => {
            return element.id == this.postId; 
        });
    }

}

window.customElements.define('post-page', PostPage);