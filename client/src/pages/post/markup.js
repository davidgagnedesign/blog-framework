export default function (viewData = null) {
    return /* html */`
        <section>
            <h2>${viewData.title}</h2>
            <div>${viewData.content}</div>
        </section>
    `;
}