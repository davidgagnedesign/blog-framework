import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';
import posts from '../../../static/data/posts';

class ArticlesPage extends HTMLElement {
    constructor() {
        super();
        InitTemplate(this);
    }

    buildElement() {
        
        let viewData = {
            posts: this.getArticles()
        }

        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getArticles() {
        return posts;
    }

}

window.customElements.define('articles-page', ArticlesPage);