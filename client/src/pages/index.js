import { InitTemplate } from '../core/template';

class Router extends HTMLElement {
    constructor() {
        super();
        this.isInit = false;
    }

    static get observedAttributes() {
        return ['route'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        this.route = newValue;
        if (this.route.indexOf('post') !== -1) this.handleSinglePostRoute(this.route);
        switch (this.route) {
            case 'about':
            case 'home':
            case 'articles':
                this.pageMarkup = `<${this.route}-page></${this.route}-page>`;
                break;
            case 'post':
                this.pageMarkup = `<${this.route}-page post-id="${this.postId}"></${this.route}-page>`;
                break;
            default:
                this.pageMarkup = `<home-page></home-page>`;  
                break;
        }
        !this.isInit ? this.initPageElement() : this.changePage();
    }

    handleSinglePostRoute(postRoute) {
        let params = postRoute.split('/');
        this.route = 'post';
        this.postId = params[2]
    }

    initPageElement() {
        InitTemplate(this);
        this.isInit = true;
    }

    changePage() {
        this.shadowRoot.innerHTML = this.buildElement();
    }

    buildElement() {
        return this.pageMarkup;
    }

}

window.customElements.define('page-router', Router);