import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';

class PostCard extends HTMLElement {
    constructor() {
        super();
        this.isInit = false;
        this.data = {};
    }

    static get observedAttributes() {
        return ['title', 'postdate', 'shortcontent', 'postid'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        this.data[name] = newValue; 
        !this.isInit ? this.initElement() : this.shadowRoot.innerHTML = this.buildElement();
    }

    initElement() {
        InitTemplate(this);
        this.isInit = true;
    }

    buildElement() {
        
        let viewData = this.data;

        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

}

window.customElements.define('post-card', PostCard);