import dateFormat from 'dateformat';

export default function (viewData = null) {
    return /* html */`
        <div class="post-card--container">
            <h3>${viewData.title}</h3>
            <p>${dateFormat(viewData.postdate, 'fullDate')}</p>
            <div>${viewData.shortcontent}</div>
            <blog-link url="/post/${viewData.postid}" text="View post"></blog-link>
        </div>
    `;
}