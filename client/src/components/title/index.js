import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';

class BlogTitle extends HTMLElement {
    constructor() {
        super();
        InitTemplate(this);
    }

    buildElement() {
        let viewData = {
            title: this.getData()
        }

        return `
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getData() {
        return 'Just another developer blog';
    }
}

window.customElements.define('blog-title', BlogTitle);