export default function (viewData = null) {
    return /* html */`
        <h1 class="main-title">${viewData.title}</h1>
    `;
}