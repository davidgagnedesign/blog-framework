export default function (viewData = null) {
    return /* html */`
        <a href="${viewData.url}">${viewData.text}</a>
    `;
}