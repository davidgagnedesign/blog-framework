import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';
import { changeRoute } from '../../core/router';

class Link extends HTMLElement {
    constructor() {
        super();
        this.isInit = false;
        this.url = null;
        this.text = null;
        this.bindEvent();
    }

    static get observedAttributes() {
        return ['url', 'text'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'url') this.url = newValue;
        if (name === 'text') this.text = newValue;
        !this.isInit ? this.initElement() : this.shadowRoot.innerHTML = this.buildElement();
    }

    initElement() {
        InitTemplate(this);
        this.isInit = true;
    }

    buildElement() {
        
        let viewData = {
            url: this.url,
            text: this.text
        };

        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    bindEvent() {
        this.addEventListener('click', (event) => {
            event.preventDefault();
            changeRoute(this.url);
        });
    }

}

window.customElements.define('blog-link', Link);