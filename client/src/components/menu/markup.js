export default function (viewData = null) {
    return /* html */`
        <nav>
            ${viewData.menuElements.map((item) => `<blog-link url="${item.href}" text="${item.name}"></blog-link>`).join('')}
        </nav>
    `;
}