import { InitTemplate } from '../../core/template';
import Style from './style';
import Markup from './markup';

class Navigation extends HTMLElement {
    constructor() {
        super();
        InitTemplate(this);
    }

    buildElement() {
        let viewData = {
            menuElements: this.getMenuElements(),
        }
        
        return /* html */`
            ${Style}
            ${Markup(viewData)}
        `;
    }

    getMenuElements() {
        return [
            {name: 'Home', href: '/'},
            {name: 'About', href: 'about'},
            {name: 'Articles', href: 'articles'}
        ];
    }
}

window.customElements.define('blog-nav', Navigation);