export function InitTemplate(componentInstance) {
    componentInstance.template = document.createElement('template');
    componentInstance.template.innerHTML = componentInstance.buildElement();
    let shadowRoot = componentInstance.attachShadow({mode: 'open'});
    shadowRoot.appendChild(componentInstance.template.content.cloneNode(true));
}
