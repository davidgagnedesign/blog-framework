export function changeRoute(route) {
    let router = document.querySelector('page-router');
    router.setAttribute('route', route);
    window.history.pushState({}, '', route);
}

export function handleFirstLoad() {
    document.addEventListener('DOMContentLoaded', (event) => {
        let urlParams = window.location.pathname.split('/');
        if (urlParams[1].length > 0) changeRoute(urlParams[1]);
    });
}