/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./client/src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./client/src/components/links/index.js":
/*!**********************************************!*\
  !*** ./client/src/components/links/index.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/components/links/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/components/links/markup.js\");\n/* harmony import */ var _core_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/router */ \"./client/src/core/router.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\n\nvar Link =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(Link, _HTMLElement);\n\n  function Link() {\n    var _this;\n\n    _classCallCheck(this, Link);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(Link).call(this));\n    _this.isInit = false;\n    _this.url = null;\n    _this.text = null;\n\n    _this.bindEvent();\n\n    return _this;\n  }\n\n  _createClass(Link, [{\n    key: \"attributeChangedCallback\",\n    value: function attributeChangedCallback(name, oldValue, newValue) {\n      if (name === 'url') this.url = newValue;\n      if (name === 'text') this.text = newValue;\n      !this.isInit ? this.initElement() : this.shadowRoot.innerHTML = this.buildElement();\n    }\n  }, {\n    key: \"initElement\",\n    value: function initElement() {\n      Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(this);\n      this.isInit = true;\n    }\n  }, {\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = {\n        url: this.url,\n        text: this.text\n      };\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }, {\n    key: \"bindEvent\",\n    value: function bindEvent() {\n      var _this2 = this;\n\n      this.addEventListener('click', function (event) {\n        event.preventDefault();\n        Object(_core_router__WEBPACK_IMPORTED_MODULE_3__[\"changeRoute\"])(_this2.url);\n      });\n    }\n  }], [{\n    key: \"observedAttributes\",\n    get: function get() {\n      return ['url', 'text'];\n    }\n  }]);\n\n  return Link;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('blog-link', Link);\n\n//# sourceURL=webpack:///./client/src/components/links/index.js?");

/***/ }),

/***/ "./client/src/components/links/markup.js":
/*!***********************************************!*\
  !*** ./client/src/components/links/markup.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <a href=\\\"\".concat(viewData.url, \"\\\">\").concat(viewData.text, \"</a>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/components/links/markup.js?");

/***/ }),

/***/ "./client/src/components/links/style.js":
/*!**********************************************!*\
  !*** ./client/src/components/links/style.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        a {\\n            font-weight: bold\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/components/links/style.js?");

/***/ }),

/***/ "./client/src/components/menu/index.js":
/*!*********************************************!*\
  !*** ./client/src/components/menu/index.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/components/menu/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/components/menu/markup.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\nvar Navigation =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(Navigation, _HTMLElement);\n\n  function Navigation() {\n    var _this;\n\n    _classCallCheck(this, Navigation);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(Navigation).call(this));\n    Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(_assertThisInitialized(_assertThisInitialized(_this)));\n    return _this;\n  }\n\n  _createClass(Navigation, [{\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = {\n        menuElements: this.getMenuElements()\n      };\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }, {\n    key: \"getMenuElements\",\n    value: function getMenuElements() {\n      return [{\n        name: 'Home',\n        href: '/'\n      }, {\n        name: 'About',\n        href: 'about'\n      }, {\n        name: 'Articles',\n        href: 'articles'\n      }];\n    }\n  }]);\n\n  return Navigation;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('blog-nav', Navigation);\n\n//# sourceURL=webpack:///./client/src/components/menu/index.js?");

/***/ }),

/***/ "./client/src/components/menu/markup.js":
/*!**********************************************!*\
  !*** ./client/src/components/menu/markup.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <nav>\\n            \".concat(viewData.menuElements.map(function (item) {\n      return \"<blog-link url=\\\"\".concat(item.href, \"\\\" text=\\\"\").concat(item.name, \"\\\"></blog-link>\");\n    }).join(''), \"\\n        </nav>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/components/menu/markup.js?");

/***/ }),

/***/ "./client/src/components/menu/style.js":
/*!*********************************************!*\
  !*** ./client/src/components/menu/style.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        a {\\n            margin-right: 2rem;\\n            text-decoration: none;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/components/menu/style.js?");

/***/ }),

/***/ "./client/src/components/post-card/index.js":
/*!**************************************************!*\
  !*** ./client/src/components/post-card/index.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/components/post-card/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/components/post-card/markup.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\nvar PostCard =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(PostCard, _HTMLElement);\n\n  function PostCard() {\n    var _this;\n\n    _classCallCheck(this, PostCard);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(PostCard).call(this));\n    _this.isInit = false;\n    _this.data = {};\n    return _this;\n  }\n\n  _createClass(PostCard, [{\n    key: \"attributeChangedCallback\",\n    value: function attributeChangedCallback(name, oldValue, newValue) {\n      this.data[name] = newValue;\n      !this.isInit ? this.initElement() : this.shadowRoot.innerHTML = this.buildElement();\n    }\n  }, {\n    key: \"initElement\",\n    value: function initElement() {\n      Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(this);\n      this.isInit = true;\n    }\n  }, {\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = this.data;\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }], [{\n    key: \"observedAttributes\",\n    get: function get() {\n      return ['title', 'postdate', 'shortcontent', 'postid'];\n    }\n  }]);\n\n  return PostCard;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('post-card', PostCard);\n\n//# sourceURL=webpack:///./client/src/components/post-card/index.js?");

/***/ }),

/***/ "./client/src/components/post-card/markup.js":
/*!***************************************************!*\
  !*** ./client/src/components/post-card/markup.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var dateformat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dateformat */ \"./node_modules/dateformat/lib/dateformat.js\");\n/* harmony import */ var dateformat__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(dateformat__WEBPACK_IMPORTED_MODULE_0__);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <div class=\\\"post-card--container\\\">\\n            <h3>\".concat(viewData.title, \"</h3>\\n            <p>\").concat(dateformat__WEBPACK_IMPORTED_MODULE_0___default()(viewData.postdate, 'fullDate'), \"</p>\\n            <div>\").concat(viewData.shortcontent, \"</div>\\n            <blog-link url=\\\"/post/\").concat(viewData.postid, \"\\\" text=\\\"View post\\\"></blog-link>\\n        </div>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/components/post-card/markup.js?");

/***/ }),

/***/ "./client/src/components/post-card/style.js":
/*!**************************************************!*\
  !*** ./client/src/components/post-card/style.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        p {\\n            color: red;\\n        }\\n        .post-card--container {\\n            width: 33%;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/components/post-card/style.js?");

/***/ }),

/***/ "./client/src/components/title/index.js":
/*!**********************************************!*\
  !*** ./client/src/components/title/index.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/components/title/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/components/title/markup.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\nvar BlogTitle =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(BlogTitle, _HTMLElement);\n\n  function BlogTitle() {\n    var _this;\n\n    _classCallCheck(this, BlogTitle);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(BlogTitle).call(this));\n    Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(_assertThisInitialized(_assertThisInitialized(_this)));\n    return _this;\n  }\n\n  _createClass(BlogTitle, [{\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = {\n        title: this.getData()\n      };\n      return \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \");\n    }\n  }, {\n    key: \"getData\",\n    value: function getData() {\n      return 'Just another developer blog';\n    }\n  }]);\n\n  return BlogTitle;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('blog-title', BlogTitle);\n\n//# sourceURL=webpack:///./client/src/components/title/index.js?");

/***/ }),

/***/ "./client/src/components/title/markup.js":
/*!***********************************************!*\
  !*** ./client/src/components/title/markup.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <h1 class=\\\"main-title\\\">\".concat(viewData.title, \"</h1>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/components/title/markup.js?");

/***/ }),

/***/ "./client/src/components/title/style.js":
/*!**********************************************!*\
  !*** ./client/src/components/title/style.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        h1 {\\n            color: green;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/components/title/style.js?");

/***/ }),

/***/ "./client/src/core/router.js":
/*!***********************************!*\
  !*** ./client/src/core/router.js ***!
  \***********************************/
/*! exports provided: changeRoute, handleFirstLoad */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changeRoute\", function() { return changeRoute; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"handleFirstLoad\", function() { return handleFirstLoad; });\nfunction changeRoute(route) {\n  var router = document.querySelector('page-router');\n  router.setAttribute('route', route);\n  window.history.pushState({}, '', route);\n}\nfunction handleFirstLoad() {\n  document.addEventListener('DOMContentLoaded', function (event) {\n    var urlParams = window.location.pathname.split('/');\n    if (urlParams[1].length > 0) changeRoute(urlParams[1]);\n  });\n}\n\n//# sourceURL=webpack:///./client/src/core/router.js?");

/***/ }),

/***/ "./client/src/core/template.js":
/*!*************************************!*\
  !*** ./client/src/core/template.js ***!
  \*************************************/
/*! exports provided: InitTemplate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"InitTemplate\", function() { return InitTemplate; });\nfunction InitTemplate(componentInstance) {\n  componentInstance.template = document.createElement('template');\n  componentInstance.template.innerHTML = componentInstance.buildElement();\n  var shadowRoot = componentInstance.attachShadow({\n    mode: 'open'\n  });\n  shadowRoot.appendChild(componentInstance.template.content.cloneNode(true));\n}\n\n//# sourceURL=webpack:///./client/src/core/template.js?");

/***/ }),

/***/ "./client/src/index.js":
/*!*****************************!*\
  !*** ./client/src/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/router */ \"./client/src/core/router.js\");\n/* harmony import */ var _components_title__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/title */ \"./client/src/components/title/index.js\");\n/* harmony import */ var _components_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/menu */ \"./client/src/components/menu/index.js\");\n/* harmony import */ var _components_links__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/links */ \"./client/src/components/links/index.js\");\n/* harmony import */ var _components_post_card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/post-card */ \"./client/src/components/post-card/index.js\");\n/* harmony import */ var _pages_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/index */ \"./client/src/pages/index.js\");\n/* harmony import */ var _pages_home__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/home */ \"./client/src/pages/home/index.js\");\n/* harmony import */ var _pages_about__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/about */ \"./client/src/pages/about/index.js\");\n/* harmony import */ var _pages_articles__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/articles */ \"./client/src/pages/articles/index.js\");\n/* harmony import */ var _pages_post__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/post */ \"./client/src/pages/post/index.js\");\n\n\n\n\n\n\n\n\n\n\nObject(_core_router__WEBPACK_IMPORTED_MODULE_0__[\"handleFirstLoad\"])();\n\n//# sourceURL=webpack:///./client/src/index.js?");

/***/ }),

/***/ "./client/src/pages/about/index.js":
/*!*****************************************!*\
  !*** ./client/src/pages/about/index.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/pages/about/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/pages/about/markup.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\nvar AboutPage =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(AboutPage, _HTMLElement);\n\n  function AboutPage() {\n    var _this;\n\n    _classCallCheck(this, AboutPage);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(AboutPage).call(this));\n    Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(_assertThisInitialized(_assertThisInitialized(_this)));\n    return _this;\n  }\n\n  _createClass(AboutPage, [{\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = {\n        text: this.getText()\n      };\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }, {\n    key: \"getText\",\n    value: function getText() {\n      return 'Text for the about page';\n    }\n  }]);\n\n  return AboutPage;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('about-page', AboutPage);\n\n//# sourceURL=webpack:///./client/src/pages/about/index.js?");

/***/ }),

/***/ "./client/src/pages/about/markup.js":
/*!******************************************!*\
  !*** ./client/src/pages/about/markup.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <section>\".concat(viewData.text, \"</section>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/pages/about/markup.js?");

/***/ }),

/***/ "./client/src/pages/about/style.js":
/*!*****************************************!*\
  !*** ./client/src/pages/about/style.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        section: {\\n            margin: 1rem 0;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/pages/about/style.js?");

/***/ }),

/***/ "./client/src/pages/articles/index.js":
/*!********************************************!*\
  !*** ./client/src/pages/articles/index.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/pages/articles/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/pages/articles/markup.js\");\n/* harmony import */ var _static_data_posts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/data/posts */ \"./client/static/data/posts.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\n\nvar ArticlesPage =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(ArticlesPage, _HTMLElement);\n\n  function ArticlesPage() {\n    var _this;\n\n    _classCallCheck(this, ArticlesPage);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(ArticlesPage).call(this));\n    Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(_assertThisInitialized(_assertThisInitialized(_this)));\n    return _this;\n  }\n\n  _createClass(ArticlesPage, [{\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = {\n        posts: this.getArticles()\n      };\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }, {\n    key: \"getArticles\",\n    value: function getArticles() {\n      return _static_data_posts__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n    }\n  }]);\n\n  return ArticlesPage;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('articles-page', ArticlesPage);\n\n//# sourceURL=webpack:///./client/src/pages/articles/index.js?");

/***/ }),

/***/ "./client/src/pages/articles/markup.js":
/*!*********************************************!*\
  !*** ./client/src/pages/articles/markup.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n    <section id=\\\"posts\\\">\\n        \".concat(viewData.posts.map(function (item, i) {\n      return \"\\n            <post-card\\n                postid=\\\"\".concat(item.id, \"\\\" \\n                title=\\\"\").concat(item.title, \"\\\"\\n                shortcontent=\\\"\").concat(item.shortContent, \"\\\" \\n                postdate=\\\"\").concat(item.published, \"\\\">\\n            </post-card>\\n        \");\n    }).join(''), \"\\n    </section>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/pages/articles/markup.js?");

/***/ }),

/***/ "./client/src/pages/articles/style.js":
/*!********************************************!*\
  !*** ./client/src/pages/articles/style.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        section: {\\n            margin: 1rem 0;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/pages/articles/style.js?");

/***/ }),

/***/ "./client/src/pages/home/index.js":
/*!****************************************!*\
  !*** ./client/src/pages/home/index.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _static_data_posts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../static/data/posts */ \"./client/static/data/posts.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style */ \"./client/src/pages/home/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./markup */ \"./client/src/pages/home/markup.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\n\nvar HomePage =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(HomePage, _HTMLElement);\n\n  function HomePage() {\n    var _this;\n\n    _classCallCheck(this, HomePage);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(HomePage).call(this));\n    Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(_assertThisInitialized(_assertThisInitialized(_this)));\n    return _this;\n  }\n\n  _createClass(HomePage, [{\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = {\n        presentation: this.getPresentationText(),\n        lastestPost: this.getLatestPosts()\n      };\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_2__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_3__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }, {\n    key: \"getPresentationText\",\n    value: function getPresentationText() {\n      return 'This blog was developed to explore the capabilities of custom web elements and Progressive web app. It been used as a learning tool and maybe in the future will become a real blog framework that is lightweight and versatile.';\n    }\n  }, {\n    key: \"getLatestPosts\",\n    value: function getLatestPosts() {\n      var limit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 3;\n      return _static_data_posts__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\n    }\n  }]);\n\n  return HomePage;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('home-page', HomePage);\n\n//# sourceURL=webpack:///./client/src/pages/home/index.js?");

/***/ }),

/***/ "./client/src/pages/home/markup.js":
/*!*****************************************!*\
  !*** ./client/src/pages/home/markup.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <section id=\\\"presentation\\\">\\n            \".concat(viewData.presentation, \"\\n        </section>\\n        <section id=\\\"posts\\\">\\n            \").concat(viewData.lastestPost.map(function (item, i) {\n      return \"\\n                <post-card\\n                    postid=\\\"\".concat(item.id, \"\\\" \\n                    title=\\\"\").concat(item.title, \"\\\"\\n                    shortcontent=\\\"\").concat(item.shortContent, \"\\\" \\n                    postdate=\\\"\").concat(item.published, \"\\\">\\n                </post-card>\\n            \");\n    }).join(''), \"\\n        </section>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/pages/home/markup.js?");

/***/ }),

/***/ "./client/src/pages/home/style.js":
/*!****************************************!*\
  !*** ./client/src/pages/home/style.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        section {\\n            margin: 1rem 0;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/pages/home/style.js?");

/***/ }),

/***/ "./client/src/pages/index.js":
/*!***********************************!*\
  !*** ./client/src/pages/index.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/template */ \"./client/src/core/template.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\nvar Router =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(Router, _HTMLElement);\n\n  function Router() {\n    var _this;\n\n    _classCallCheck(this, Router);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(Router).call(this));\n    _this.isInit = false;\n    return _this;\n  }\n\n  _createClass(Router, [{\n    key: \"attributeChangedCallback\",\n    value: function attributeChangedCallback(name, oldValue, newValue) {\n      this.route = newValue;\n      if (this.route.indexOf('post') !== -1) this.handleSinglePostRoute(this.route);\n\n      switch (this.route) {\n        case 'about':\n        case 'home':\n        case 'articles':\n          this.pageMarkup = \"<\".concat(this.route, \"-page></\").concat(this.route, \"-page>\");\n          break;\n\n        case 'post':\n          this.pageMarkup = \"<\".concat(this.route, \"-page post-id=\\\"\").concat(this.postId, \"\\\"></\").concat(this.route, \"-page>\");\n          break;\n\n        default:\n          this.pageMarkup = \"<home-page></home-page>\";\n          break;\n      }\n\n      !this.isInit ? this.initPageElement() : this.changePage();\n    }\n  }, {\n    key: \"handleSinglePostRoute\",\n    value: function handleSinglePostRoute(postRoute) {\n      var params = postRoute.split('/');\n      this.route = 'post';\n      this.postId = params[2];\n    }\n  }, {\n    key: \"initPageElement\",\n    value: function initPageElement() {\n      Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(this);\n      this.isInit = true;\n    }\n  }, {\n    key: \"changePage\",\n    value: function changePage() {\n      this.shadowRoot.innerHTML = this.buildElement();\n    }\n  }, {\n    key: \"buildElement\",\n    value: function buildElement() {\n      return this.pageMarkup;\n    }\n  }], [{\n    key: \"observedAttributes\",\n    get: function get() {\n      return ['route'];\n    }\n  }]);\n\n  return Router;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('page-router', Router);\n\n//# sourceURL=webpack:///./client/src/pages/index.js?");

/***/ }),

/***/ "./client/src/pages/post/index.js":
/*!****************************************!*\
  !*** ./client/src/pages/post/index.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _core_template__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/template */ \"./client/src/core/template.js\");\n/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ \"./client/src/pages/post/style.js\");\n/* harmony import */ var _markup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./markup */ \"./client/src/pages/post/markup.js\");\n/* harmony import */ var _static_data_posts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../static/data/posts */ \"./client/static/data/posts.js\");\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _wrapNativeSuper(Class) { var _cache = typeof Map === \"function\" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== \"function\") { throw new TypeError(\"Super expression must either be null or a function\"); } if (typeof _cache !== \"undefined\") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }\n\nfunction isNativeReflectConstruct() { if (typeof Reflect === \"undefined\" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === \"function\") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }\n\nfunction _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }\n\nfunction _isNativeFunction(fn) { return Function.toString.call(fn).indexOf(\"[native code]\") !== -1; }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\n\n\n\n\n\nvar PostPage =\n/*#__PURE__*/\nfunction (_HTMLElement) {\n  _inherits(PostPage, _HTMLElement);\n\n  function PostPage() {\n    var _this;\n\n    _classCallCheck(this, PostPage);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(PostPage).call(this));\n    _this.isInit = false;\n    return _this;\n  }\n\n  _createClass(PostPage, [{\n    key: \"attributeChangedCallback\",\n    value: function attributeChangedCallback(name, oldValue, newValue) {\n      if (name === 'post-id') this.postId = newValue;\n      !this.isInit ? this.initElement() : this.shadowRoot.innerHTML = this.buildElement();\n    }\n  }, {\n    key: \"initElement\",\n    value: function initElement() {\n      Object(_core_template__WEBPACK_IMPORTED_MODULE_0__[\"InitTemplate\"])(this);\n      this.isInit = true;\n    }\n  }, {\n    key: \"buildElement\",\n    value: function buildElement() {\n      var viewData = this.getText();\n      return (\n        /* html */\n        \"\\n            \".concat(_style__WEBPACK_IMPORTED_MODULE_1__[\"default\"], \"\\n            \").concat(Object(_markup__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(viewData), \"\\n        \")\n      );\n    }\n  }, {\n    key: \"getText\",\n    value: function getText() {\n      var _this2 = this;\n\n      return _static_data_posts__WEBPACK_IMPORTED_MODULE_3__[\"default\"].find(function (element) {\n        return element.id == _this2.postId;\n      });\n    }\n  }], [{\n    key: \"observedAttributes\",\n    get: function get() {\n      return ['post-id'];\n    }\n  }]);\n\n  return PostPage;\n}(_wrapNativeSuper(HTMLElement));\n\nwindow.customElements.define('post-page', PostPage);\n\n//# sourceURL=webpack:///./client/src/pages/post/index.js?");

/***/ }),

/***/ "./client/src/pages/post/markup.js":
/*!*****************************************!*\
  !*** ./client/src/pages/post/markup.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  var viewData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;\n  return (\n    /* html */\n    \"\\n        <section>\\n            <h2>\".concat(viewData.title, \"</h2>\\n            <div>\").concat(viewData.content, \"</div>\\n        </section>\\n    \")\n  );\n});\n\n//# sourceURL=webpack:///./client/src/pages/post/markup.js?");

/***/ }),

/***/ "./client/src/pages/post/style.js":
/*!****************************************!*\
  !*** ./client/src/pages/post/style.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (/* html */\"\\n    <style>\\n        section: {\\n            margin: 1rem 0;\\n        }\\n    </style>\\n\");\n\n//# sourceURL=webpack:///./client/src/pages/post/style.js?");

/***/ }),

/***/ "./client/static/data/posts.js":
/*!*************************************!*\
  !*** ./client/static/data/posts.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar posts = [{\n  id: 1,\n  title: 'Stop cheering tech companies as they were sports teams',\n  published: '2019-02-24T19:27:19.728Z',\n  shortContent: \"<p>Lorem ipsum dolor sit amet. ullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam se</p>\",\n  content:\n  /* html */\n  \"\\n            <p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse scelerisque mauris augue, ac luctus eros gravida ornare. Etiam fermentum ut nibh ut commodo. Quisque posuere, lorem ac efficitur tincidunt, ipsum tortor maximus tortor, ac commodo metus felis laoreet magna. Donec id nisi vitae nisi aliquam finibus. Ut in sem sit amet nisl aliquam malesuada id quis erat. In convallis lorem ut ornare venenatis. Sed consequat velit tincidunt semper viverra.</p>\\n            <p>Nullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam sed odio suscipit accumsan. Nulla in sem in ante luctus feugiat vitae nec erat. Curabitur ex ex, venenatis eu convallis nec, ullamcorper elementum erat. Suspendisse eu aliquet elit. Vestibulum in erat ac libero vulputate egestas quis scelerisque nunc. Praesent malesuada porttitor leo sit amet tincidunt. Nam suscipit id leo bibendum laoreet. Donec justo lacus, faucibus sed tempor sed, tincidunt vel libero. Etiam in libero ornare, aliquet ex ac, tempor nunc.</p>\\n        \"\n}, {\n  id: 2,\n  title: 'If your diss legacy code you spit on your job',\n  published: '2019-02-24T19:27:58.355Z',\n  shortContent: \"<p>Lorem ipsum dolor sit amet. ullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam se</p>\",\n  content:\n  /* html */\n  \"\\n            <p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse scelerisque mauris augue, ac luctus eros gravida ornare. Etiam fermentum ut nibh ut commodo. Quisque posuere, lorem ac efficitur tincidunt, ipsum tortor maximus tortor, ac commodo metus felis laoreet magna. Donec id nisi vitae nisi aliquam finibus. Ut in sem sit amet nisl aliquam malesuada id quis erat. In convallis lorem ut ornare venenatis. Sed consequat velit tincidunt semper viverra.</p>\\n            <p>Nullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam sed odio suscipit accumsan. Nulla in sem in ante luctus feugiat vitae nec erat. Curabitur ex ex, venenatis eu convallis nec, ullamcorper elementum erat. Suspendisse eu aliquet elit. Vestibulum in erat ac libero vulputate egestas quis scelerisque nunc. Praesent malesuada porttitor leo sit amet tincidunt. Nam suscipit id leo bibendum laoreet. Donec justo lacus, faucibus sed tempor sed, tincidunt vel libero. Etiam in libero ornare, aliquet ex ac, tempor nunc.</p>\\n        \"\n}, {\n  id: 3,\n  title: 'You know all the new framework, so what ?',\n  published: '2019-02-24T19:28:07.356Z',\n  shortContent: \"<p>Lorem ipsum dolor sit amet. ullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam se</p>\",\n  content:\n  /* html */\n  \"\\n            <p>3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse scelerisque mauris augue, ac luctus eros gravida ornare. Etiam fermentum ut nibh ut commodo. Quisque posuere, lorem ac efficitur tincidunt, ipsum tortor maximus tortor, ac commodo metus felis laoreet magna. Donec id nisi vitae nisi aliquam finibus. Ut in sem sit amet nisl aliquam malesuada id quis erat. In convallis lorem ut ornare venenatis. Sed consequat velit tincidunt semper viverra.</p>\\n            <p>Nullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam sed odio suscipit accumsan. Nulla in sem in ante luctus feugiat vitae nec erat. Curabitur ex ex, venenatis eu convallis nec, ullamcorper elementum erat. Suspendisse eu aliquet elit. Vestibulum in erat ac libero vulputate egestas quis scelerisque nunc. Praesent malesuada porttitor leo sit amet tincidunt. Nam suscipit id leo bibendum laoreet. Donec justo lacus, faucibus sed tempor sed, tincidunt vel libero. Etiam in libero ornare, aliquet ex ac, tempor nunc.</p>\\n        \"\n}];\n/* harmony default export */ __webpack_exports__[\"default\"] = (posts);\n\n//# sourceURL=webpack:///./client/static/data/posts.js?");

/***/ }),

/***/ "./node_modules/dateformat/lib/dateformat.js":
/*!***************************************************!*\
  !*** ./node_modules/dateformat/lib/dateformat.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var __WEBPACK_AMD_DEFINE_RESULT__;/*\n * Date Format 1.2.3\n * (c) 2007-2009 Steven Levithan <stevenlevithan.com>\n * MIT license\n *\n * Includes enhancements by Scott Trenda <scott.trenda.net>\n * and Kris Kowal <cixar.com/~kris.kowal/>\n *\n * Accepts a date, a mask, or a date and a mask.\n * Returns a formatted version of the given date.\n * The date defaults to the current date/time.\n * The mask defaults to dateFormat.masks.default.\n */\n\n(function(global) {\n  'use strict';\n\n  var dateFormat = (function() {\n      var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\\1?|[LloSZWN]|\"[^\"]*\"|'[^']*'/g;\n      var timezone = /\\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\\d{4})?)\\b/g;\n      var timezoneClip = /[^-+\\dA-Z]/g;\n  \n      // Regexes and supporting functions are cached through closure\n      return function (date, mask, utc, gmt) {\n  \n        // You can't provide utc if you skip other args (use the 'UTC:' mask prefix)\n        if (arguments.length === 1 && kindOf(date) === 'string' && !/\\d/.test(date)) {\n          mask = date;\n          date = undefined;\n        }\n  \n        date = date || new Date;\n  \n        if(!(date instanceof Date)) {\n          date = new Date(date);\n        }\n  \n        if (isNaN(date)) {\n          throw TypeError('Invalid date');\n        }\n  \n        mask = String(dateFormat.masks[mask] || mask || dateFormat.masks['default']);\n  \n        // Allow setting the utc/gmt argument via the mask\n        var maskSlice = mask.slice(0, 4);\n        if (maskSlice === 'UTC:' || maskSlice === 'GMT:') {\n          mask = mask.slice(4);\n          utc = true;\n          if (maskSlice === 'GMT:') {\n            gmt = true;\n          }\n        }\n  \n        var _ = utc ? 'getUTC' : 'get';\n        var d = date[_ + 'Date']();\n        var D = date[_ + 'Day']();\n        var m = date[_ + 'Month']();\n        var y = date[_ + 'FullYear']();\n        var H = date[_ + 'Hours']();\n        var M = date[_ + 'Minutes']();\n        var s = date[_ + 'Seconds']();\n        var L = date[_ + 'Milliseconds']();\n        var o = utc ? 0 : date.getTimezoneOffset();\n        var W = getWeek(date);\n        var N = getDayOfWeek(date);\n        var flags = {\n          d:    d,\n          dd:   pad(d),\n          ddd:  dateFormat.i18n.dayNames[D],\n          dddd: dateFormat.i18n.dayNames[D + 7],\n          m:    m + 1,\n          mm:   pad(m + 1),\n          mmm:  dateFormat.i18n.monthNames[m],\n          mmmm: dateFormat.i18n.monthNames[m + 12],\n          yy:   String(y).slice(2),\n          yyyy: y,\n          h:    H % 12 || 12,\n          hh:   pad(H % 12 || 12),\n          H:    H,\n          HH:   pad(H),\n          M:    M,\n          MM:   pad(M),\n          s:    s,\n          ss:   pad(s),\n          l:    pad(L, 3),\n          L:    pad(Math.round(L / 10)),\n          t:    H < 12 ? dateFormat.i18n.timeNames[0] : dateFormat.i18n.timeNames[1],\n          tt:   H < 12 ? dateFormat.i18n.timeNames[2] : dateFormat.i18n.timeNames[3],\n          T:    H < 12 ? dateFormat.i18n.timeNames[4] : dateFormat.i18n.timeNames[5],\n          TT:   H < 12 ? dateFormat.i18n.timeNames[6] : dateFormat.i18n.timeNames[7],\n          Z:    gmt ? 'GMT' : utc ? 'UTC' : (String(date).match(timezone) || ['']).pop().replace(timezoneClip, ''),\n          o:    (o > 0 ? '-' : '+') + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),\n          S:    ['th', 'st', 'nd', 'rd'][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10],\n          W:    W,\n          N:    N\n        };\n  \n        return mask.replace(token, function (match) {\n          if (match in flags) {\n            return flags[match];\n          }\n          return match.slice(1, match.length - 1);\n        });\n      };\n    })();\n\n  dateFormat.masks = {\n    'default':               'ddd mmm dd yyyy HH:MM:ss',\n    'shortDate':             'm/d/yy',\n    'mediumDate':            'mmm d, yyyy',\n    'longDate':              'mmmm d, yyyy',\n    'fullDate':              'dddd, mmmm d, yyyy',\n    'shortTime':             'h:MM TT',\n    'mediumTime':            'h:MM:ss TT',\n    'longTime':              'h:MM:ss TT Z',\n    'isoDate':               'yyyy-mm-dd',\n    'isoTime':               'HH:MM:ss',\n    'isoDateTime':           'yyyy-mm-dd\\'T\\'HH:MM:sso',\n    'isoUtcDateTime':        'UTC:yyyy-mm-dd\\'T\\'HH:MM:ss\\'Z\\'',\n    'expiresHeaderFormat':   'ddd, dd mmm yyyy HH:MM:ss Z'\n  };\n\n  // Internationalization strings\n  dateFormat.i18n = {\n    dayNames: [\n      'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',\n      'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'\n    ],\n    monthNames: [\n      'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',\n      'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'\n    ],\n    timeNames: [\n      'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'\n    ]\n  };\n\nfunction pad(val, len) {\n  val = String(val);\n  len = len || 2;\n  while (val.length < len) {\n    val = '0' + val;\n  }\n  return val;\n}\n\n/**\n * Get the ISO 8601 week number\n * Based on comments from\n * http://techblog.procurios.nl/k/n618/news/view/33796/14863/Calculate-ISO-8601-week-and-year-in-javascript.html\n *\n * @param  {Object} `date`\n * @return {Number}\n */\nfunction getWeek(date) {\n  // Remove time components of date\n  var targetThursday = new Date(date.getFullYear(), date.getMonth(), date.getDate());\n\n  // Change date to Thursday same week\n  targetThursday.setDate(targetThursday.getDate() - ((targetThursday.getDay() + 6) % 7) + 3);\n\n  // Take January 4th as it is always in week 1 (see ISO 8601)\n  var firstThursday = new Date(targetThursday.getFullYear(), 0, 4);\n\n  // Change date to Thursday same week\n  firstThursday.setDate(firstThursday.getDate() - ((firstThursday.getDay() + 6) % 7) + 3);\n\n  // Check if daylight-saving-time-switch occurred and correct for it\n  var ds = targetThursday.getTimezoneOffset() - firstThursday.getTimezoneOffset();\n  targetThursday.setHours(targetThursday.getHours() - ds);\n\n  // Number of weeks between target Thursday and first Thursday\n  var weekDiff = (targetThursday - firstThursday) / (86400000*7);\n  return 1 + Math.floor(weekDiff);\n}\n\n/**\n * Get ISO-8601 numeric representation of the day of the week\n * 1 (for Monday) through 7 (for Sunday)\n * \n * @param  {Object} `date`\n * @return {Number}\n */\nfunction getDayOfWeek(date) {\n  var dow = date.getDay();\n  if(dow === 0) {\n    dow = 7;\n  }\n  return dow;\n}\n\n/**\n * kind-of shortcut\n * @param  {*} val\n * @return {String}\n */\nfunction kindOf(val) {\n  if (val === null) {\n    return 'null';\n  }\n\n  if (val === undefined) {\n    return 'undefined';\n  }\n\n  if (typeof val !== 'object') {\n    return typeof val;\n  }\n\n  if (Array.isArray(val)) {\n    return 'array';\n  }\n\n  return {}.toString.call(val)\n    .slice(8, -1).toLowerCase();\n};\n\n\n\n  if (true) {\n    !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {\n      return dateFormat;\n    }).call(exports, __webpack_require__, exports, module),\n\t\t\t\t__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));\n  } else {}\n})(this);\n\n\n//# sourceURL=webpack:///./node_modules/dateformat/lib/dateformat.js?");

/***/ })

/******/ });