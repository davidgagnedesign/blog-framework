let posts = [
    {
        id: 1,
        title: 'Stop cheering tech companies as they were sports teams',
        published: '2019-02-24T19:27:19.728Z',
        shortContent: `<p>Lorem ipsum dolor sit amet. ullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam se</p>`,
        content: /* html */`
            <p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse scelerisque mauris augue, ac luctus eros gravida ornare. Etiam fermentum ut nibh ut commodo. Quisque posuere, lorem ac efficitur tincidunt, ipsum tortor maximus tortor, ac commodo metus felis laoreet magna. Donec id nisi vitae nisi aliquam finibus. Ut in sem sit amet nisl aliquam malesuada id quis erat. In convallis lorem ut ornare venenatis. Sed consequat velit tincidunt semper viverra.</p>
            <p>Nullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam sed odio suscipit accumsan. Nulla in sem in ante luctus feugiat vitae nec erat. Curabitur ex ex, venenatis eu convallis nec, ullamcorper elementum erat. Suspendisse eu aliquet elit. Vestibulum in erat ac libero vulputate egestas quis scelerisque nunc. Praesent malesuada porttitor leo sit amet tincidunt. Nam suscipit id leo bibendum laoreet. Donec justo lacus, faucibus sed tempor sed, tincidunt vel libero. Etiam in libero ornare, aliquet ex ac, tempor nunc.</p>
        `,
    },
    {
        id: 2,
        title: 'If your diss legacy code you spit on your job',
        published: '2019-02-24T19:27:58.355Z',
        shortContent: `<p>Lorem ipsum dolor sit amet. ullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam se</p>`,
        content: /* html */`
            <p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse scelerisque mauris augue, ac luctus eros gravida ornare. Etiam fermentum ut nibh ut commodo. Quisque posuere, lorem ac efficitur tincidunt, ipsum tortor maximus tortor, ac commodo metus felis laoreet magna. Donec id nisi vitae nisi aliquam finibus. Ut in sem sit amet nisl aliquam malesuada id quis erat. In convallis lorem ut ornare venenatis. Sed consequat velit tincidunt semper viverra.</p>
            <p>Nullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam sed odio suscipit accumsan. Nulla in sem in ante luctus feugiat vitae nec erat. Curabitur ex ex, venenatis eu convallis nec, ullamcorper elementum erat. Suspendisse eu aliquet elit. Vestibulum in erat ac libero vulputate egestas quis scelerisque nunc. Praesent malesuada porttitor leo sit amet tincidunt. Nam suscipit id leo bibendum laoreet. Donec justo lacus, faucibus sed tempor sed, tincidunt vel libero. Etiam in libero ornare, aliquet ex ac, tempor nunc.</p>
        `,
    },
    {
        id: 3,
        title: 'You know all the new framework, so what ?',
        published: '2019-02-24T19:28:07.356Z',
        shortContent: `<p>Lorem ipsum dolor sit amet. ullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam se</p>`,
        content: /* html */`
            <p>3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse scelerisque mauris augue, ac luctus eros gravida ornare. Etiam fermentum ut nibh ut commodo. Quisque posuere, lorem ac efficitur tincidunt, ipsum tortor maximus tortor, ac commodo metus felis laoreet magna. Donec id nisi vitae nisi aliquam finibus. Ut in sem sit amet nisl aliquam malesuada id quis erat. In convallis lorem ut ornare venenatis. Sed consequat velit tincidunt semper viverra.</p>
            <p>Nullam pulvinar eros ac nunc malesuada maximus. Ut ornare volutpat fermentum. Fusce non pellentesque dolor. Morbi suscipit quam sed odio suscipit accumsan. Nulla in sem in ante luctus feugiat vitae nec erat. Curabitur ex ex, venenatis eu convallis nec, ullamcorper elementum erat. Suspendisse eu aliquet elit. Vestibulum in erat ac libero vulputate egestas quis scelerisque nunc. Praesent malesuada porttitor leo sit amet tincidunt. Nam suscipit id leo bibendum laoreet. Donec justo lacus, faucibus sed tempor sed, tincidunt vel libero. Etiam in libero ornare, aliquet ex ac, tempor nunc.</p>
        `,
    },
];

export default posts;